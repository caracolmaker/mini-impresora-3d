Mini impresora 3d inspirada en el proyecto de JC_3DESING (podeis ver el proyecto en su pagina web [aqui](https://jc3design.es/))
Muchas gracias a JC 3DESING por compartir su proyecto ya que estoy aprendiendo mucho.
Este proyecto esta hecho desde 0 con FreeCad y no usa ningun archivo del otro proyecto aunque si he tomado medidas y demas para construir 
este proyecto. Ya que mi hija queria una impresora para ella sola y asi poderse imprimir sus muñecos.

La impresora esta en proceso de desarrollo todavia.

Caracteristicas:

1.  Volumen de impresion 180x180x180.
2.  Refuerzos en el eje Y.
3.  Eje Z e Y inspirado en la prusa mini.
4.  Refuerzo eje Z con un tubo de acero 25x25mm.
5.  Extrusor bowden.
6.  Cama caliente a 220 V (opcional).
7.  Ramps 1.6 con Fuente de alimentacion de 12V y 10A.
8.  Tensor eje Y.


